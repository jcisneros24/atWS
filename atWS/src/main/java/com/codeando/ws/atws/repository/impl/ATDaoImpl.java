package com.codeando.ws.atws.repository.impl;

import java.sql.SQLException;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.ProductDto;
import com.codeando.ws.atws.dto.UserDto;
import com.codeando.ws.atws.repository.ATDao;

@Repository
public class ATDaoImpl implements ATDao {

	final static Logger logger = LoggerFactory.getLogger(ATDaoImpl.class);

	@Autowired
	private DataSource dataSource;

	@Override
	public String updateProduct(ProductDto request) throws SQLException {
		return null;
	}

	@Override
	public String deleteProduct(ProductDto request) {
		return null;
	}

	@Override
	public RespuestaBean createUser(UserDto request) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDto readUser(String userRq) throws SQLException {

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);

		String sql = "SELECT USER, PASSWORD FROM ATDB.USERS WHERE USER = '" + userRq + "'";

		UserDto user = (UserDto) jdbcTemplate.queryForObject(sql, UserDto.class);

		return user;
	}

	@Override
	public RespuestaBean updateUser(UserDto request) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaBean deleteUser(String userRq) throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}

}
