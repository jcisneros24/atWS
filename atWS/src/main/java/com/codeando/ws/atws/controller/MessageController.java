package com.codeando.ws.atws.controller;

import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;
import com.google.gson.Gson;

@RestController
public class MessageController {

	final static Logger logger = LoggerFactory.getLogger(MessageController.class);

	@RequestMapping(value = "/showMessage", method = RequestMethod.GET, headers = "Accept=application/json")
	public String showMessage(UserDto udto) {

		logger.info("paso 0");
		ObjectMapper mapper = new ObjectMapper();
		udto = new UserDto();
		logger.info("paso 1");
		udto.setUser("Jhonny");
		udto.setPassword("12345");

		udto.getUser();
		udto.getPassword();
		
		RespuestaBean response = new RespuestaBean();
		try {
			String JSON = mapper.writeValueAsString(udto);
			logger.info("Print JSON: " + JSON);
			response.setCode("0");
			response.setValue(JSON);
		} catch (Exception e) {
			response.setCode("1");
			response.setValue("Error");
		}

		return new Gson().toJson(response);
	}

}
