package com.codeando.ws.atws.repository;

import java.sql.SQLException;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.ProductDto;
import com.codeando.ws.atws.dto.UserDto;

public interface ATDao {
	
	/* Tabla: PRODUCTS */
	String updateProduct(ProductDto request) throws SQLException;
	String deleteProduct(ProductDto request) throws SQLException;
	
	/* Tabla: USERS */
	RespuestaBean createUser(UserDto request)throws SQLException;
	UserDto readUser(String userRq)throws SQLException;
	RespuestaBean updateUser(UserDto request)throws SQLException;
	RespuestaBean deleteUser(String userRq) throws SQLException;
}
