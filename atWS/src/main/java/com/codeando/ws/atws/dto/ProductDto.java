package com.codeando.ws.atws.dto;

import java.io.Serializable;

public class ProductDto implements Serializable {

	private static final long serialVersionUID = 1L;

	private String referenceOld;
	private String referenceNew;

	public String getReferenceOld() {
		return referenceOld;
	}

	public void setReferenceOld(String referenceOld) {
		this.referenceOld = referenceOld;
	}

	public String getReferenceNew() {
		return referenceNew;
	}

	public void setReferenceNew(String referenceNew) {
		this.referenceNew = referenceNew;
	}
}
