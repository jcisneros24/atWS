package com.codeando.ws.atws.service;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;

public interface LoginService {
	
	UserDto findUserById(int id);
	RespuestaBean validateUser(String user, String password);
	
}
