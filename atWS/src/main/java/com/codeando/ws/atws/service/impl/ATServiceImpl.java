package com.codeando.ws.atws.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;
import com.codeando.ws.atws.service.ATService;
import com.codeando.ws.atws.util.Constante;

@Service
public class ATServiceImpl implements ATService{

	final static Logger logger = LoggerFactory.getLogger(ATServiceImpl.class);
	
	public RespuestaBean showMessage(String user){
		
		RespuestaBean response = new RespuestaBean();
		
		response.setCode(Constante.PARAMETRO.COD_RESPUESTA_EXITO);
		response.setValue(Constante.MENSAJE.MSJ_RESPUESTA_EXITO +" "+user);
		
		return response;
	}

	@Override
	public List<UserDto> showAllUsers() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public UserDto showUser(String userRq) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
