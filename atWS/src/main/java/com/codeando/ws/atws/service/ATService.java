package com.codeando.ws.atws.service;

import java.util.List;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;

public interface ATService {
	
	RespuestaBean showMessage(String user);
	List<UserDto> showAllUsers();
	UserDto showUser(String userRq);
}
