package com.codeando.ws.atws.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;
import com.codeando.ws.atws.service.LoginService;
import com.google.gson.Gson;

@RestController
public class LoginController {

	final static Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private LoginService loginService;

	@RequestMapping(value = "/logincheck", method = RequestMethod.POST, headers = "Accept=application/json")
	public String loginCheck(UserDto userDto){
		
		logger.info("Inicia validacion de no nulos usuario y password desde controller.");
		RespuestaBean response = new RespuestaBean();
		String user = userDto.getUser();
		String password = userDto.getPassword();
		if(user != null && password != null){
			
			response = loginService.validateUser(user, password);

		} else {
			response.setCode("0");
			response.setValue("Usuario o Password no son validos.");
		}
		
		return new Gson().toJson(response);
	}

}
