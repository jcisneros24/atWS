package com.codeando.ws.atws.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.dto.UserDto;
import com.codeando.ws.atws.service.LoginService;

@Service
public class LoginServiceImpl implements LoginService{
	
	final static Logger logger = LoggerFactory.getLogger(LoginServiceImpl.class);
	
	@Override
	public UserDto findUserById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public RespuestaBean validateUser(String user, String password) {
		
		logger.info("Inicia validación de usuario y password desde service.");
		RespuestaBean response = new RespuestaBean();
		String userDB = "jca";
		String passwordDB = "jca";
		
		if (user.equals(userDB) && password.equals(passwordDB)) {
			
			response.setCode("1");
			response.setValue("Validación de usuario y password exitosa.");
		} else {
			response.setCode("0");
			response.setValue("Validación de usuario y password no exitosa.");
		}
		
		return response;
	}
	
}
