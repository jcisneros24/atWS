package com.codeando.ws.atws.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.codeando.ws.atws.bean.RespuestaBean;
import com.codeando.ws.atws.service.ATService;

@RestController
public class ATController {

	final static Logger logger = LoggerFactory.getLogger(ATController.class);
	
	@Autowired
	ATService atService;
	
	@RequestMapping(value = "/showMessageInit", method = RequestMethod.POST, headers = "Accept=application/json", produces = "text/plain;charset=UTF-8")
	public String showMessageInit(@RequestBody String name) {
		
		RespuestaBean response = new RespuestaBean();
		
		System.out.println(name);
		
		response.setValue(name);
		response.getValue();
		
		System.out.println("bean: "+response.getValue());
		
		return "Hola "+response.getValue();
	}
	
	/*@RequestMapping(value = "/showMessageError", method = RequestMethod.POST, headers = "Accept=application/json", produces = "application/json")
	public RespuestaBean showMessageUser(@RequestBody String userRq)
            throws ServletException, IOException {
		RespuestaBean response = new RespuestaBean();
//		String user = userRq.getParameter("user");
		String user = userRq;
		if(StringUtils.isBlank(user)){
//			return new ModelAndView("redirect:/cotizador-vehicular");
			response.setCode(Constante.PARAMETRO.COD_RESPUESTA_NO_EXITO);
			response.setValue(Constante.MENSAJE.MSJ_RESPUESTA_NO_EXITO);
		} else {
			response = atService.showMessage(user);
		}
		
		return response;
	}*/
	
}
